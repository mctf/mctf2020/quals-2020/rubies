FROM ruby:2.5.1

ARG RUBY_FLAG

RUN echo $RUBY_FLAG > /flag_with_complicated_name.txt && chmod 444 /flag_with_complicated_name.txt
ADD . /app/
WORKDIR /app
RUN apt-get update && apt-get install -y nodejs
RUN ["gem", "install", "bundler:2.1.4"]
RUN ["bin/bundle", "install"]
RUN ["bin/rails", "db:migrate", "RAILS_ENV=production"]
ENTRYPOINT ["rails", "s", "-b", "0.0.0.0", "-e", "production"]
