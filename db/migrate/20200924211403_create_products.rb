class CreateProducts < ActiveRecord::Migration[5.2]
  def self.up
    create_table :products do |t|
      t.string :title
      t.string :image
      t.integer :price

      t.timestamps
    end

    Product.create title: 'A very nice diamond ruby',
                   image: 'https://st3.depositphotos.com/12851394/15375/i/1600/depositphotos_153753158-stock-photo-red-ruby-diamond-on-the.jpg',
                   price: 100_000

    Product.create title: 'A ruby so nice I almost cried while making this task',
                   image: 'https://media.gettyimages.com/photos/ruby-gemstone-picture-id168250055?s=612x612',
                   price: 300_000

    Product.create title: 'Please buy our rubies',
                   image: 'https://media.gettyimages.com/photos/large-ruby-stone-picture-id183295467?s=612x612',
                   price: 120_000

    Product.create title: 'If you buy this ruby, we\'ll even give you 300$ worth of rails as a gift from our partner "RzhD"',
                   image: 'https://st.depositphotos.com/1018166/2231/i/450/depositphotos_22314923-stock-photo-ruby-heart.jpg',
                   price: 50_000
  end

  def self.down
    drop_table :products
  end
end
