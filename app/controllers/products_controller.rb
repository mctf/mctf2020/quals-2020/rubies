class ProductsController < ApplicationController
  def index
    @products = Product.all
    render file: 'products/index'
  end

  def show
    @product = Product.find(params[:id])
    render file: 'products/show'
  end
end
