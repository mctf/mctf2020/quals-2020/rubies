# Be sure to restart your server when you modify this file.

# Version of your assets_bkp, change this if you want to expire all your assets_bkp.
Rails.application.config.assets.version = '1.0'

# Add additional assets_bkp to the asset load path.
# Rails.application.config.assets_bkp.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets_bkp.
# application.js, application.css, and all non-JS/CSS in the app/assets_bkp
# folder are already added.
# Rails.application.config.assets_bkp.precompile += %w( admin.js admin.css )
